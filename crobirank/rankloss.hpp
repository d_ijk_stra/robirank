/*
 * Copyright (c) 2014 Hyokun Yun, Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
#ifndef _RANKLOSS_HPP_
#define _RANKLOSS_HPP_

#include "appctx.hpp"

namespace rankloss{
  
  PetscErrorCode robirank(PetscScalar *fx_array, 
                          PetscScalar *labels_array, 
                          PetscScalar *grad_array, 
                          PetscInt    n, 
                          PetscScalar *loss,
                          AppCtx      *user);
  
  PetscErrorCode infinitepush(PetscScalar *fx_array, 
                              PetscScalar *labels_array, 
                              PetscScalar *grad_array, 
                              PetscInt    n, 
                              PetscScalar *loss,
                              AppCtx      *user);
  
  PetscErrorCode logistic(PetscScalar *fx_array, 
                          PetscScalar *labels_array, 
                          PetscScalar *grad_array, 
                          PetscInt    n, 
                          PetscScalar *loss,
                          AppCtx      *user);
  
  PetscErrorCode identity(PetscScalar *fx_array, 
                          PetscScalar *labels_array, 
                          PetscScalar *grad_array, 
                          PetscInt    n, 
                          PetscScalar *loss,
                          AppCtx      *user);
  
  PetscErrorCode tlogistic(PetscScalar *fx_array, 
                          PetscScalar *labels_array, 
                          PetscScalar *grad_array, 
                          PetscInt    n, 
                          PetscScalar *loss,
                          AppCtx      *user);
  
  
  PetscErrorCode init(AppCtx *user);
  
  PetscErrorCode finalize(void);

  double rank(PetscInt doc, 
              vector<PetscInt> doc_group, 
              PetscScalar *fx_array, 
              PetscScalar *labels_array, 
              PetscScalar *grad_array,
              PetscScalar c_i,
              PetscInt m,
              PetscScalar alpha);
  
  double rank_identity(PetscInt doc, 
              vector<PetscInt> doc_group, 
              PetscScalar *fx_array, 
              PetscScalar *labels_array, 
              PetscScalar *grad_array,
              PetscScalar c_i,
              PetscInt m,
              PetscScalar alpha);
  
  double rank_tlogistic(PetscInt doc, 
              vector<PetscInt> doc_group, 
              PetscScalar *fx_array, 
              PetscScalar *labels_array, 
              PetscScalar *grad_array,
              PetscScalar c_i,
              PetscInt m,
              PetscScalar alpha);
  
  PetscScalar idcg(PetscInt q, 
                   vector<PetscInt> doc_group, 
                   PetscScalar *labels_array, 
                   PetscInt truncation);
  
  PetscScalar dcg(PetscInt q, 
                  vector<PetscInt> doc_group, 
                  PetscScalar *fx_array, 
                  PetscScalar *labels_array, 
                  PetscInt truncation);
  
  PetscScalar ndcg(PetscInt q, 
                   vector<PetscInt> doc_group, 
                   PetscScalar *fx_array, 
                   PetscScalar *labels_array, 
                   PetscInt truncation);
  
  PetscScalar avg_ndcg(map<PetscInt, vector<PetscInt> > gs_map, 
                       PetscScalar *fx_array, 
                       PetscScalar *labels_array, 
                       PetscInt truncation);
  double log2(double n);
}

#endif
