/*
 * Copyright (c) 2014 Hyokun Yun, Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
#ifndef _APPCTX_HPP_
#define _APPCTX_HPP_

#include "petscmat.h"
#include "timer.hpp"
#include "tao.h"
#include <map>
#include <vector>
using namespace std;

// User-defined application context - contains data needed by the
// application-provided call-back routines that evaluate the function,
// and gradient.

#define err_msg(msg)  {PetscPrintf(PETSC_COMM_WORLD, "%s\n", msg);  exit(1);}

typedef struct {

  PetscMPIInt rank;
  PetscMPIInt size;

  char w_file[PETSC_MAX_PATH_LEN]; // load w from external file

  /// training dataset and its statistics
  Vec labels;              
  Mat data;
  PetscInt dim;            // Dimensions 
  PetscInt m;              // Number of data points 

  map<PetscInt, vector<PetscInt> > gs_map;
  map<PetscInt, vector<PetscInt> > tgs_map;
  map<PetscInt, PetscScalar> cval;
  PetscInt trunc;

  PetscReal tol;

  /// Test dataset and its statistics
  Vec tlabels;             // Test labels
  Mat tdata;               // Test data
  PetscInt tdim;           // test Dimensions 
  PetscInt tm;             // Number of test data points 

  
  /// Training parameters
  PetscReal lambda;        // Regularization parameter
  PetscReal alpha;         // Weight parameter for pairwise loss
  PetscInt  loss;          // RobiRank or InfinitePush 
  PetscInt  solver;          // LBFGS or BMRM
  
  // Variables for intermediate storage 
  Vec fx;                  // Store predictions <w, x>
  Vec tfx;                 // Function value of test data
  Vec fxloc;               // locally stored fx on head node
  Vec grad;                // Store coefficients for the gradient   
  Vec gradloc;             // locally stored gradient on head node 
  
  Vec labelsloc;           // locally stored labels on head node  
  VecScatter scatter;      // Scatter context 
  
  // Time the optimization procedure 
  timer opt_timer;
  
  // Time obj and grad calculation 
  timer objgrad_timer;    // timer for computing the obj and gradient

  // Time matrix vector multiplication
  timer matvec_timer;

  PetscInt num_fn_eval;
  
} AppCtx;


// Functors for sorting increasing order 
template<typename T>
struct indirect_less_than {
  T *array;
  indirect_less_than(T *value) : array(value) {}
  bool operator() (const int &a, const int &b) {return array[a] < array[b];}
};

// Functors for sorting in decreasing order
template<typename T>
struct indirect_greater_than {
  T *array;
  indirect_greater_than(T *value) : array(value) {}
  bool operator() (const int &a, const int &b) {return array[a] > array[b];}
};


PetscErrorCode PrintSetting(AppCtx *user);

PetscErrorCode GetMachineInfo(AppCtx *user);

#endif
