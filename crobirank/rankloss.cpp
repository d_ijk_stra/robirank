/*
 * Copyright (c) 2014 Hyokun Yun, Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
#include "rankloss.hpp"
#include <algorithm>
#include <iostream>
#include <vector>
#include <cmath>
#include <cassert>
using namespace::std;

namespace rankloss{

  // loss_grad - Evaluate infinitepush rank loss 
  
  // Input Parameters:
  // fx_array - function prediction
  // labels_array - labels
  // n - size of arrays
  // user - user defined application context 

  // Output parameters:
  // grad_array - coefficients of the gradient 
  // loss function value 

  PetscErrorCode infinitepush(PetscScalar *fx_array, 
                              PetscScalar *labels_array, 
                              PetscScalar *grad_array, 
                              PetscInt    n,
                              PetscScalar *loss,
                              AppCtx      *user){
    
    fill(grad_array, grad_array+n, 0.0);
    map<PetscInt, vector<PetscInt> > &gs_map = user->gs_map;
    *loss = 0.0;
    for(map<PetscInt, vector<PetscInt> >::iterator it=gs_map.begin(); it!=gs_map.end(); ++it){
      PetscInt q = it->first;
      vector<PetscInt> doc_group = it->second;
      PetscScalar c_i = user->cval[q];
      // If the document group size is 1 then there is nothing to learn
      // ignore them 
      if(doc_group.size() == 1)
        continue;      

      // First compute loss by computing max over y' \in \Ycal_{x}
      PetscScalar max_loss_ip = -1.0;
      PetscInt max_ip = -1;
      for(PetscInt ip=0; ip<doc_group.size(); ip++){
        PetscScalar fxyp = fx_array[doc_group[ip]];
        PetscScalar loss_ip = 0.0;
        for(PetscInt i = 0; i < doc_group.size(); i++){
          PetscScalar fxy = fx_array[doc_group[i]];
          if(1.0 + fxyp > fxy){
            PetscScalar vxy = (pow(2, labels_array[doc_group[i]]) - 1.0);
            loss_ip += vxy * (1.0 - (fxy-fxyp));
          }
        }
        if(loss_ip > max_loss_ip){
          max_ip = ip;
          max_loss_ip = loss_ip;
        }
      }
      
      *loss += c_i*max_loss_ip;
      
      // Now compute the gradient
      PetscScalar fxyp = fx_array[doc_group[max_ip]];
      for(PetscInt i = 0; i < doc_group.size(); i++){
        PetscScalar fxy = fx_array[doc_group[i]];
        if(1.0 + fxyp > fxy){
          PetscScalar vxy = (pow(2, labels_array[doc_group[i]]) - 1.0);
          grad_array[doc_group[i]] -= (c_i*vxy/user->m);
          grad_array[doc_group[max_ip]] += (c_i*vxy/user->m);
        }
      }
    }
    *loss /= user->m;
    return 0;
  }

  // loss_grad - Evaluate logistic loss 
  
  // Input Parameters:
  // fx_array - function prediction
  // labels_array - labels
  // n - size of arrays
  // user - user defined application context 

  // Output parameters:
  // grad_array - coefficients of the gradient 
  // loss function value 

  PetscErrorCode logistic(PetscScalar *fx_array, 
                          PetscScalar *labels_array, 
                          PetscScalar *grad_array, 
                          PetscInt    n,
                          PetscScalar *loss,
                          AppCtx      *user){
    
    fill(grad_array, grad_array+n, 0.0);
    map<PetscInt, vector<PetscInt> > &gs_map = user->gs_map;
    *loss = 0.0;
    for(map<PetscInt, vector<PetscInt> >::iterator it=gs_map.begin(); it!=gs_map.end(); ++it){
      PetscInt q = it->first;
      vector<PetscInt> doc_group = it->second;
      PetscScalar c_i = user->cval[q];
      // If the document group size is 1 then there is nothing to learn
      // ignore them 
      if(doc_group.size() == 1)
        continue;      
      for(PetscInt i = 0; i < doc_group.size(); i++){
        PetscScalar fxy = fx_array[doc_group[i]];
        PetscScalar u_max = 0.0;
        for(PetscInt ip = 0; ip < doc_group.size(); ip++){
          PetscScalar fxyp = fx_array[doc_group[ip]];
          PetscScalar u = fxyp-fxy;
          if(u > u_max)
            u_max = u;
        }
        PetscScalar loss_i = 0.0;
        for(PetscInt ip = 0; ip < doc_group.size(); ip++){
          PetscScalar fxyp = fx_array[doc_group[ip]];
          PetscScalar u = fxyp-fxy;
          loss_i += exp(u-u_max);
        }
        PetscScalar vxy = (pow(2, labels_array[doc_group[i]]) - 1.0);
        PetscScalar offset = exp(-u_max);
        *loss += c_i*vxy*(log(offset + loss_i) + u_max); 

        PetscScalar coeff = c_i*vxy/(user->m*(offset+loss_i));
        grad_array[doc_group[i]] -= coeff*loss_i;
        
        for(PetscInt ip = 0; ip < doc_group.size(); ip++){
          PetscScalar fxyp = fx_array[doc_group[ip]];
          PetscScalar u = fxyp-fxy;
          grad_array[doc_group[ip]] += coeff*exp(u-u_max);
        }
      }
    }
    *loss /= user->m;
    return 0;
  }
 
  // loss_grad - Evaluate adentity loss 
  
  // Input Parameters:
  // fx_array - function prediction
  // labels_array - labels
  // n - size of arrays
  // user - user defined application context 

  // Output parameters:
  // grad_array - coefficients of the gradient 
  // loss function value 

  PetscErrorCode identity(PetscScalar *fx_array, 
                          PetscScalar *labels_array, 
                          PetscScalar *grad_array, 
                          PetscInt    n,
                          PetscScalar *loss,
                          AppCtx      *user){
    fill(grad_array, grad_array+n, 0.0);
    map<PetscInt, vector<PetscInt> > &gs_map = user->gs_map;
    *loss = 0.0;
    for(map<PetscInt, vector<PetscInt> >::iterator it=gs_map.begin(); it!=gs_map.end(); ++it){
      PetscInt q = it->first;
      vector<PetscInt> doc_group = it->second;
      PetscScalar c_i = user->cval[q];
      // If the document group size is 1 then there is nothing to learn
      // ignore them 
      if(doc_group.size() == 1)
        continue;      
      for(PetscInt doc=0; doc<doc_group.size(); doc++)
        *loss += c_i * user->alpha * rank_identity(doc, doc_group, fx_array, labels_array, grad_array, c_i, user->m, user->alpha)*(pow(2, labels_array[doc_group[doc]])-1.0);
      
    }
    *loss /= user->m;
    return 0;
  }

  // loss_grad - Evaluate tlogistic loss 
  
  // Input Parameters:
  // fx_array - function prediction
  // labels_array - labels
  // n - size of arrays
  // user - user defined application context 

  // Output parameters:
  // grad_array - coefficients of the gradient 
  // loss function value 

  PetscErrorCode tlogistic(PetscScalar *fx_array, 
                          PetscScalar *labels_array, 
                          PetscScalar *grad_array, 
                          PetscInt    n,
                          PetscScalar *loss,
                          AppCtx      *user){
    
    fill(grad_array, grad_array+n, 0.0);
    map<PetscInt, vector<PetscInt> > &gs_map = user->gs_map;
    *loss = 0.0;
    for(map<PetscInt, vector<PetscInt> >::iterator it=gs_map.begin(); it!=gs_map.end(); ++it){
      PetscInt q = it->first;
      vector<PetscInt> doc_group = it->second;
      PetscScalar c_i = user->cval[q];
      // If the document group size is 1 then there is nothing to learn
      // ignore them 
      if(doc_group.size() == 1)
        continue;      
      for(PetscInt doc=0; doc<doc_group.size(); doc++)
        *loss += c_i * user->alpha * rank_tlogistic(doc, doc_group, fx_array, labels_array, grad_array, c_i, user->m, user->alpha)*(pow(2, labels_array[doc_group[doc]])-1.0);
      
    }
    *loss /= user->m;
    return 0;
  }

  
  // loss_grad - Evaluate RobiRank loss 
  
  // Input Parameters:
  // fx_array - function prediction
  // labels_array - labels
  // n - size of arrays
  // user - user defined application context 

  // Output parameters:
  // grad_array - coefficients of the gradient 
  // loss function value 

  PetscErrorCode robirank(PetscScalar *fx_array, 
                          PetscScalar *labels_array, 
                          PetscScalar *grad_array, 
                          PetscInt    n,
                          PetscScalar *loss,
                          AppCtx      *user){
    
    fill(grad_array, grad_array+n, 0.0);
    map<PetscInt, vector<PetscInt> > &gs_map = user->gs_map;
    *loss = 0.0;
    for(map<PetscInt, vector<PetscInt> >::iterator it=gs_map.begin(); it!=gs_map.end(); ++it){
      PetscInt q = it->first;
      vector<PetscInt> doc_group = it->second;
      PetscScalar c_i = user->cval[q];
      // If the document group size is 1 then there is nothing to learn
      // ignore them 
      if(doc_group.size() == 1)
        continue;      
      for(PetscInt doc=0; doc<doc_group.size(); doc++)
        *loss += c_i * user->alpha * rank(doc, doc_group, fx_array, labels_array, grad_array, c_i, user->m, user->alpha)*(pow(2, labels_array[doc_group[doc]])-1.0);
      
    }
    *loss /= user->m;
    return 0;
  }
  
  PetscScalar rank(PetscInt doc, 
                   vector<PetscInt> doc_group, 
                   PetscScalar *fx_array, 
                   PetscScalar *labels_array, 
                   PetscScalar *grad_array,
                   PetscScalar c_i,
                   PetscInt m,
                   PetscScalar alpha){
    
    PetscScalar coeff = 0.0;
    PetscScalar norm = 0.0;
    PetscScalar fxy = fx_array[doc_group[doc]];
    
    for(PetscInt i=0; i<doc_group.size(); i++){
      PetscScalar u = fxy-fx_array[doc_group[i]];
      // Guard against overflow 
      if(fabs(u) == 0){
        norm += 1.0;
      }else if(u > 0.0){
        PetscScalar exp_u=exp(-u);
        norm += log2(1.0+exp_u);
      }else{
        PetscScalar exp_u=exp(u);
        norm += log2(1.0+exp_u)-(u/log(2.0));
      }
    }
    
    for(PetscInt i=0; i<doc_group.size(); i++){
      if(i==doc)    continue;      
      PetscScalar denom=1.0 + exp(fxy - fx_array[doc_group[i]]);
      coeff=(c_i * alpha)/(log(2)*log(2))*(pow(2, labels_array[doc_group[doc]]) - 1.0)/denom;
      grad_array[doc_group[doc]] -= (coeff/ (norm * m));
      grad_array[doc_group[i]] += (coeff/ (norm * m));
    }
    return log2(norm);
  }

  PetscScalar rank_identity(PetscInt doc, 
                   vector<PetscInt> doc_group, 
                   PetscScalar *fx_array, 
                   PetscScalar *labels_array, 
                   PetscScalar *grad_array,
                   PetscScalar c_i,
                   PetscInt m,
                   PetscScalar alpha){
    
    PetscScalar coeff = 0.0;
    PetscScalar norm = 0.0;
    PetscScalar fxy = fx_array[doc_group[doc]];
    
    for(PetscInt i=0; i<doc_group.size(); i++){
      if(i==doc)    continue;
      PetscScalar u = fxy-fx_array[doc_group[i]];
      // Guard against overflow 
      if(fabs(u) == 0){
        norm += 1.0;
      }else if(u > 0.0){
        PetscScalar exp_u=exp(-u);
        norm += log2(1.0+exp_u);
      }else{
        PetscScalar exp_u=exp(u);
        norm += log2(1.0+exp_u)-(u/log(2.0));
      }
    }
    
    for(PetscInt i=0; i<doc_group.size(); i++){
      if(i==doc)    continue;      
      PetscScalar denom=1.0 + exp(fxy - fx_array[doc_group[i]]);
      coeff=(c_i * alpha)/(log(2))*(pow(2, labels_array[doc_group[doc]]) - 1.0) / denom;
      grad_array[doc_group[doc]] -= (coeff/m);
      grad_array[doc_group[i]] += (coeff/m);
    }
    return norm;
  }

  PetscScalar rank_tlogistic(PetscInt doc, 
                   vector<PetscInt> doc_group, 
                   PetscScalar *fx_array, 
                   PetscScalar *labels_array, 
                   PetscScalar *grad_array,
                   PetscScalar c_i,
                   PetscInt m,
                   PetscScalar alpha){

    PetscScalar coeff = 0.0;
    PetscScalar innernorm = 0.0;
    PetscScalar outernorm = 0.0;
    PetscScalar fxy = fx_array[doc_group[doc]];
    
    for(PetscInt i=0; i<doc_group.size(); i++){
      if(i==doc)    continue;
      PetscScalar tempnorm = 0.0;
      PetscScalar u = fxy-fx_array[doc_group[i]];
      // Guard against overflow 
      if(fabs(u) == 0){
        tempnorm = 1.0;
      }
      if(fabs(u) != 0){
        if(u > 0.0){
          PetscScalar exp_u=exp(-u);
          tempnorm = log2(1.0+exp_u);
        }else{
          PetscScalar exp_u=exp(u);
          tempnorm = log2(1.0+exp_u)-(u/log(2.0));
        }
      }
      outernorm += log2(1.0+tempnorm);
    }
    
    for(PetscInt i=0; i<doc_group.size(); i++){
      if(i==doc)    continue; 
      PetscScalar denom=(1.0 + log2(1.0 + exp(fx_array[doc_group[i]] - fxy))) * (1.0 + exp(fxy - fx_array[doc_group[i]]));
      coeff=(c_i * alpha)/(log(2) * log(2)) * (pow(2, labels_array[doc_group[doc]]) - 1.0)/denom;
      grad_array[doc_group[doc]] -= (coeff/ (m));
      grad_array[doc_group[i]] += (coeff/ (m));
    }
    return outernorm;
  }

  PetscScalar idcg(PetscInt q, 
                   vector<PetscInt> doc_group, 
                   PetscScalar *labels_array, 
                   PetscInt truncation){
    vector<PetscScalar> ideal_pi;
    PetscScalar idcg = 0.0;
    PetscInt size = doc_group.size();
    PetscInt k = size > truncation? truncation: size;
    
    for(PetscInt i=0; i<size; i++)
      ideal_pi.push_back(labels_array[doc_group[i]]);
    
    sort(ideal_pi.begin(), ideal_pi.end(), greater<PetscScalar>());
    
    for(PetscInt i=0; i<k; i++)
      idcg += (pow(2, ideal_pi[i]) - 1.0)/log2(i+2.0);
    
    return idcg;
  }

  PetscScalar dcg(PetscInt q, 
                  vector<PetscInt> doc_group, 
                  PetscScalar *fx_array, 
                  PetscScalar *labels_array, 
                  PetscInt truncation){
    multimap<PetscScalar, PetscScalar, greater<PetscScalar> >  scores;
    PetscScalar dcgval = 0;
    PetscInt size = doc_group.size();
    
    for(PetscInt i=0; i<doc_group.size(); i++){
      PetscScalar score = fx_array[doc_group[i]];
      scores.insert(pair<PetscScalar, PetscScalar> (score, labels_array[doc_group[i]]));
    }
    
    PetscInt k = size > truncation? truncation: size;    
    PetscInt j = 0;
    multimap<PetscScalar, PetscScalar, greater<PetscScalar> >::iterator it;
    for(it=scores.begin(), j=0; j<k; ++j, ++it){
      dcgval += (pow(2, it->second) - 1.0)/log2(j+2.0);
    }
    return dcgval;
  }
  
  PetscScalar ndcg(PetscInt q, 
                   vector<PetscInt> doc_group, 
                   PetscScalar *fx_array, 
                   PetscScalar *labels_array, 
                   PetscInt truncation){
    PetscScalar dcgval = dcg(q, doc_group, fx_array, labels_array, truncation);
    PetscScalar idcgval = idcg(q, doc_group, labels_array, truncation);
    return dcgval/idcgval;
  }
  
  PetscScalar avg_ndcg(map<PetscInt, vector<PetscInt> > gs_map, 
                       PetscScalar *fx_array, 
                       PetscScalar *labels_array, 
                       PetscInt truncation){
    PetscScalar avg_ndcg = 0.0;
    PetscScalar result = 0.0;

    for(map<PetscInt, vector<PetscInt> >::iterator it=gs_map.begin(); it!=gs_map.end(); ++it){
      PetscInt q = it->first;
      vector<PetscInt> doc_group = it->second;
      result = ndcg(q, doc_group, fx_array, labels_array, truncation);
      avg_ndcg += result;
    } 
    
    return avg_ndcg/gs_map.size();
  }
  
  PetscErrorCode init(AppCtx *user){ 
    PetscErrorCode info;
    PetscScalar *labels_array;
    map<PetscInt, vector<PetscInt> > &gs_map = user->gs_map;
    info = VecGetArray(user->labels, &labels_array); CHKERRQ(info);
    for(map<PetscInt, vector<PetscInt> >::iterator it=gs_map.begin(); it!=gs_map.end(); ++it){
      PetscInt q = it->first;
      vector<PetscInt> doc_group = it->second;
      user->cval[q] = 1.0/idcg(q, doc_group, labels_array, user->trunc);
    }    
    info = VecRestoreArray(user->labels, &labels_array); CHKERRQ(info);
    return 0;
  }

  PetscErrorCode finalize(void){
    return 0;
  }

  PetscScalar log2(PetscScalar n){
    return log(n) / log(2.0);
  }
}
